#!/bin/sh
CLUSTER="../c2"
DIR=$CLUSTER/$(hostname)/

mkdir -p $DIR
apt install hardinfo
apt install inxi
apt install cpuid
cat /proc/cpuinfo > $DIR/cpuinfo
uname -a > $DIR/uname
ifconfig > $DIR/ifconfig
hostname > $DIR/hostname
lspci > $DIR/lspci
lscpu > $DIR/lscpu
cpuid > $DIR/cpuid
dmidecode --type processor > $DIR/dmidecode
dmidecode -t 17 processor > $DIR/dmidecode17
inxi -F > $DIR/inxi
lshw -html CPU > $DIR/lshw.html
hardinfo > $DIR/hardinfo
