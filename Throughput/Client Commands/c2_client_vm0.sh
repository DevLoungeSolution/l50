#!/bin/bash
CLUSTER="../../c2"
DIR=$CLUSTER/$(hostname)/Lab3
declare -a IPLIST=("10.0.0.6" "10.0.0.7" "10.0.0.8" "10.0.0.4")
rm -rf $DIR
mkdir -p $DIR
for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/1/$ip/"
for (( a=0; a<5; a++ )) 
do
iperf -c $ip -t 10 -i 1 -f g | grep -e "bits/sec" -m 10 >> "$DIR/1/$ip/exp1"_"$a"
done
done


for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/2/$ip/"
for ((a=0; a<5; a++))
do
iperf -c $ip -t 10 -i 1 -f g -d | grep -e "bits/sec" -e "with" -m 22 >> "$DIR/2/$ip/exp2"_"$a"
done
done


for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/3/$ip/"
for w in 10 50 100 150 200 250
do
for ((a=0; a<5; a++))
do
iperf -c $ip -t 3 -i 1 -f k -w $w'KB'| grep -e "window" -e "bits/sec">>"$DIR/3/$ip/exp3_$w"_"$a"
done
done
done


for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/4/$ip/"
for b in 10 100 1000 5000 9000
do
for ((a=0; a<5; a++))
do
iperf -c $ip -t 3 -i 1 -f k -w $b"KB"| grep -e "window" -e "bits/sec">>"$DIR/4/$ip/exp_$b"_"$a"
done
done
done
