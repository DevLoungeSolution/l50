#!/bin/bash
CLUSTER="c1"
DIR=$CLUSTER/$(hostname)/Lab3
declare -a IPLIST=("10.0.6" "10.0.0.7" "10.0.0.8" "10.0.0.4")
rm -rf $DIR/4/


for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/4/$ip/"
for b in 10 100 1000 5000 9000
do
for ((a=0; a<5; a++))
do
iperf -c $ip -t 3 -i 1 -f m -b   $b"m" | grep -e "%" >>"$DIR/exp_$b_$a"
done
done
done
