#!/bin/bash
CLUSTER="c1"
DIR=$CLUSTER/$(hostname)/Lab3
declare -a IPLIST=("10.0.6" "10.0.0.7" "10.0.0.8" "10.0.0.4")
rm -rf $DIR/2/

for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/2/$ip/"
for ((a=0; a<5; a++))
do
iperf -c $ip -t 10 -i 1 -f g -d | grep -e "bits/sec" -e "with" -m 22 >> "$DIR/2/$ip/exp2"_"$a"
done
done

