#!/bin/bash
CLUSTER="../c2"
DIR=$CLUSTER/$(hostname)/
declare -a IPLIST=("10.0.0.6" "10.0.0.7" "10.0.0.8" "10.0.0.4")
rm -r "$DIR/Latency"
mkdir -p "$DIR/Latency/1/"
mkdir -p "$DIR/Latency/2/"
mkdir -p "$DIR/Latency/3/"
mkdir -p "$DIR/Latency/4/"
mkdir -p "$DIR/Latency/5/"

for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/Latency/1/$ip/"
for (( c=0; c<10; c++ ))
do
ping $ip -c 500 -i 0.001 | grep -e "tt"l >> "$DIR/Latency/1/$ip/exp1_$c"
done
done

for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/Latency/2/$ip/"
for (( i=0; i<10; i++))
do
ping $ip -c 1000 -i 0.001 | grep -e "ttl" >> "$DIR/Latency/2/$ip/exp2_$i"
done
done

for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/Latency/3/$ip/"
for i in 0.000001   0.00001   0.0001  0.001  0.01  0.1
do
   ping $ip -c 500 -i $i | grep -e "tt"l >> "$DIR/Latency/3/$ip/exp2_$i"
done
done

for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/Latency/4/$ip/"
ping $ip -c 10000 -i 0 | grep -e "transmitted" -e "rtt" >> "$DIR/Latency/4/$ip/exp3_$ip"
done

for ip in ${IPLIST[@]}
do
mkdir -p "$DIR/Latency/5/$ip/"
taskset -c 0 ping $ip -c 10000 -i 0 |grep -e "transmitted" -e "rtt">>"$DIR/Latency/5/$ip/exp4_$ip"
done 

